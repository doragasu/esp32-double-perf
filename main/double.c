#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <esp_system.h>
#include <esp_timer.h>

#define ITERATIONS 1000000

IRAM_ATTR void str_to_double(void)
{
	char *end;

	for (int i = 0; i < ITERATIONS; i++) {
		volatile double num = strtod("1", &end);
		(num);
	}
}

IRAM_ATTR void str_to_long(void)
{
	char *end;

	for (int i = 0; i < ITERATIONS; i++) {
		volatile long num = strtol("1", &end, 10);
		(num);
	}
}

IRAM_ATTR static void double_to_str(void)
{
	char str[32];

	for (int i = 0; i < ITERATIONS; i++) {
		snprintf(str, sizeof(str), "%f", 1.0);
	}
}

IRAM_ATTR static void int_to_str(void)
{
	char str[32];

	for (int i = 0; i < ITERATIONS; i++) {
		snprintf(str, sizeof(str), "%d", 1);
	}
}

static const char *fn_str[] = {
	"str_to_double", "str_to_long", "double_to_str", "int_to_str"
};

IRAM_ATTR static void (*fn[])(void) = {
	str_to_double, str_to_long, double_to_str, int_to_str
};

IRAM_ATTR void app_main(void)
{
	int64_t start, end;

	for (int i = 0; i < 4; i++) {
		printf("Testing %s...", fn_str[i]);
		start = esp_timer_get_time();
		fn[i]();
		end = esp_timer_get_time();
		printf(" done: %"PRIu64" us\n", end - start);
	}
}
