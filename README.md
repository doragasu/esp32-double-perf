# Introduction

Recently while browsing [cJSON library code](https://github.com/DaveGamble/cJSON), I frowned when I saw its parser always converts numbers to double and then the double to integer. No matter the number has or not decimal digits, the double conversion is always performed. Conversely `cJSON_Print()` functions always prints the double value even if the input is an integer.

I know handling doubles is usually slower on embedded targets, but how much can it be on devices like ESP32 with a single precision FPU?

To test it, I wrote this dumb program, that test performance of comversions from int/double to strings and from strings to int/double. Each test function does 1 million iterations of the conversion to test, and the time it takes to perform these iterations is printed on the debug trace. Debug functions are placed in IRAM to avoid delays introduced by cache misses.

# Results

When running the program as is (that converts number 1 using all test functions), I get these results on a ESP32 running at the default 160 MHz, and with the code built using esp-idf 5.1.1:

```
Testing str_to_double... done: 2358279 us
Testing str_to_long... done: 1651520 us
Testing double_to_str... done: 59963801 us
Testing int_to_str... done: 4572552 us
```

Conversion from string to double is about 42.8% slower than string to int, in line with what I would expect (it is slower but not way slower since ESP32 has a single precision FPU). A bit more surprising is the conversion from double to string that is a whopping 1311.4% slower than conversion from int to string (it takes 13 times as much to do the conversion).

# Conclusion

While working on embedded targets like the ESP32, most of the times I deal with numbers in JSON format, these numbers are integer. As the tests have shown, conversions dealing with double are more expensive than integer conversions (specially the double to string is 13x slower in the test), thus treating integer numbers as doubles can introduce a severe performance penalty. Thus it would be better for cJSON library to handle floating point and integer numbers separately (using different types maybe), specially for the case of the `cJSON_Print()` functions, that are waaaaaay slower with doubles.
